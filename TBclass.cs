﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace ОЛН
{
    public partial class TBclass : System.Windows.Forms.TextBox
    {
        private string answer; //поле класса 1
        private int n;
        private TextBox[] tb; //поле класса 2
       // public string answer1;

       
        public TextBox[] TB_ //Cвойство для доступа к полю класса 
        {
            get //Если отсутствует часть set, свойство доступно только для чтения 
            {
                return tb;
            }
        }
        public TBclass(int n, int w, int h, String answer) //конструктор с параметрами 
        {
           //answer1 = "";
            this.answer = answer;
            this.n = n;
            tb = new TextBox[n];
            for (int i = 0; i < n; i++)
            {
                tb[i] = new TextBox();
                tb[i].Location = new Point(w + i * 16, h);
                tb[i].Name = "textbox" + i.ToString();
                tb[i].Size = new Size(16, 20);
                tb[i].TextAlign = HorizontalAlignment.Center;
                tb[i].MaxLength = 1;
                tb[i].Tag = "group" + this.GetHashCode();
                tb[i].TextChanged += new EventHandler(tbTextChanged);//Данное событие возникает в том случае, если свойство Text изменено программой или в результате действий пользователя.
                tb[i].KeyDown += new KeyEventHandler(tbKeyDown);//Происходит при нажатии клавиши, если элемент управления имеет фокус.
                tb[i].KeyPress += new KeyPressEventHandler(tbKeyPress);//Событие KeyPress вызывается только нажатием клавиш с символами. Остальные клавиши вызывают события KeyDown и KeyUp.
                tb[i].Click += new EventHandler(tbClick);//Происходит при щелчке мышью по текстбоксу
                
              }
        }


        //public void tbhighlight()
        //{
        //    foreach (TextBox textBox in this.TB_)
        //    {
        //        textBox.BackColor = Color.Red;
        //    }
        //}//tbhighlight

        public bool tbcheck()
        {
            if (this.answer == null) return true;

            String text = this.getStringData();

            return answer.ToUpper() == text;
        }//tbcheck

        public String getStringData()
        {
            String data = "";

            foreach (TextBox textBox in this.TB_)
            {
                data += textBox.Text;
            }

            return data;
        }//getStringData

        public void setStringData(string data)
        {
            char[] split = new char[n];
            for (int i = 0; i < data.Length; i++)
            {
                split[i] = data[i];

            }
            int j = 0;
            foreach (TextBox textBox in this.TB_)
            {
                textBox.Text = split[j].ToString().ToUpper();

                j++;
            }

        }//setStringData

        public void tbClick(object sender, EventArgs e)
        {
            tb[Array.IndexOf(tb, sender)].SelectionStart = tb[Array.IndexOf(tb, sender)].Text.Length;
            tb[Array.IndexOf(tb, sender)].Focus();

        }//tbClick

        public void tbKeyPress(object sender, KeyPressEventArgs e)//Происходит при нажатии клавиши с буквой,пробела или клавиши BACKSPACE, если фокус находится в элементе управления.
        {

            e.KeyChar = char.ToUpper(e.KeyChar);

        }//tbKeyPress

        public void tbTextChanged(object sender, EventArgs e)
        {
            if (tb[Array.IndexOf(tb, sender)].Text == "") return; //sender - это указатель на объект вызвавший это событие.
            //если элемент в строке последний
            if (Array.IndexOf(tb, sender) == tb.GetUpperBound(0)) return;//GetUpperBound - Получает Индекс последнего элемента заданного измерения в массиве.
            tb[Array.IndexOf(tb, sender) + 1].Focus();
        }//tbTextChanged

        public void tbKeyDown(object sender, KeyEventArgs e) //Происходит при нажатии клавиши, если элемент управления имеет фокус.
        {
            
            switch (e.KeyCode)
            {
                case Keys.Left:

                    if (Array.IndexOf(tb, sender) == 0)
                    {
                        tb[Array.IndexOf(tb, sender)].Focus();
                        tb[Array.IndexOf(tb, sender)].Select(1, 0);
                         break;
                    }
                    tb[Array.IndexOf(tb, sender) - 1].Focus(); 
                    break;
                  

                case Keys.Right:
                    if (Array.IndexOf(tb, sender) == tb.GetUpperBound(0))
                        break;
                    tb[Array.IndexOf(tb, sender) + 1].Focus();
                        break;

                case Keys.Back:
                    if (Array.IndexOf(tb, sender) == 0) break;

                    if (tb[Array.IndexOf(tb, sender)].Text == "")
                    {
                        tb[Array.IndexOf(tb, sender) - 1].Text = null;
                        tb[Array.IndexOf(tb, sender) - 1].Focus();
                        break;
                    }
                    else
                    {
                        tb[Array.IndexOf(tb, sender)].Text = null;
                        tb[Array.IndexOf(tb, sender)].Focus();
                        break;
                    }

                case Keys.Delete:
                    if (Array.IndexOf(tb, sender) == tb.GetUpperBound(0))
                    {
                        tb[Array.IndexOf(tb, sender)].Text = null;
                        break;
                    }
                    if (tb[Array.IndexOf(tb, sender)].Text == "")
                    {
                        tb[Array.IndexOf(tb, sender) + 1].Focus();
                        break;
                    }
                    else
                    {
                        tb[Array.IndexOf(tb, sender)].Text = null;
                        tb[Array.IndexOf(tb, sender) + 1].Focus();
                        break;
                    }


            }


            

        }//tbKeyDown


    }
}
