﻿namespace ОЛН
{
    partial class Component1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbFullConrtol1 = new ОЛН.TBFullConrtol();
            this.treeView1 = new System.Windows.Forms.TreeView();
            // 
            // tbFullConrtol1
            // 
            this.tbFullConrtol1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbFullConrtol1.AutoScroll = true;
            this.tbFullConrtol1.Location = new System.Drawing.Point(7, 75);
            this.tbFullConrtol1.MinimumSize = new System.Drawing.Size(861, 0);
            this.tbFullConrtol1.Name = "tbFullConrtol1";
            this.tbFullConrtol1.Size = new System.Drawing.Size(882, 542);
            this.tbFullConrtol1.TabIndex = 1;
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(301, 621);
            this.treeView1.TabIndex = 0;

        }

        #endregion

        private TBFullConrtol tbFullConrtol1;
        private System.Windows.Forms.TreeView treeView1;
    }
}
